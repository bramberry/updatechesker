-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema update_checker
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema update_checker
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `update_checker`
  DEFAULT CHARACTER SET utf8;
USE `update_checker`;

-- -----------------------------------------------------
-- Table `update_checker`.`computer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `update_checker`.`computer` (
  `id`              INT          NOT NULL AUTO_INCREMENT,
  `audience_number` VARCHAR(10)  NULL,
  `ip`              VARCHAR(15)  NULL,
  `MAC`             VARCHAR(17)  NULL,
  `discription`     VARCHAR(100) NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE`
  ON `update_checker`.`computer` (`id` ASC);

-- -----------------------------------------------------
-- Table `update_checker`.`hardware`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `update_checker`.`hardware` (
  `id`                  INT          NOT NULL AUTO_INCREMENT,
  `CPU`                 VARCHAR(45)  NULL,
  `ram_volume`          INT          NULL,
  `ram_technical_speed` VARCHAR(45)  NULL,
  `hdd_volume`          INT          NULL,
  `hdd_model`           VARCHAR(45)  NULL,
  `hdd_fragmentation`   VARCHAR(45)  NULL,
  `comment`             VARCHAR(100) NULL,
  `computer_id`         INT          NOT NULL,
  PRIMARY KEY (`id`, `computer_id`),
  CONSTRAINT `fk_hardware_computer`
  FOREIGN KEY (`computer_id`)
  REFERENCES `update_checker`.`computer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

CREATE INDEX `fk_hardware_computer_idx`
  ON `update_checker`.`hardware` (`computer_id` ASC);

-- -----------------------------------------------------
-- Table `update_checker`.`software`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `update_checker`.`software` (
  `id`               INT         NOT NULL,
  `name`             VARCHAR(45) NULL,
  `current_version`  VARCHAR(45) NULL,
  `previous_version` VARCHAR(45) NULL,
  `install_date`     DATE        NULL,
  `update_date`      DATE        NULL,
  `computer_id`      INT         NOT NULL,
  PRIMARY KEY (`id`, `computer_id`),
  CONSTRAINT `fk_software_computer1`
  FOREIGN KEY (`computer_id`)
  REFERENCES `update_checker`.`computer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

CREATE INDEX `fk_software_computer1_idx`
  ON `update_checker`.`software` (`computer_id` ASC);


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
