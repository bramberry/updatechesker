package by.vsu.bramberry.updateChesker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpdateCheskerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpdateCheskerApplication.class, args);
    }
}
