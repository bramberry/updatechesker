package by.vsu.bramberry.updateChesker.controllers;

import by.vsu.bramberry.updateChesker.services.TransmitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    private final TransmitterService transmitterService;

    @Autowired
    public MainController(TransmitterService transmitterService) {
        this.transmitterService = transmitterService;
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String indexController() {

        return null;
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public String getAll() {
        transmitterService.transmitAll();
        return null;
    }

    @RequestMapping(value = "/getone", method = RequestMethod.GET)
    public String getOne(@RequestParam(value = "127.0.0.1") String ip) {
        transmitterService.transmit(ip);
        return null;
    }
}
