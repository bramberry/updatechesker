package by.vsu.bramberry.updateChesker.model.transfer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.Callable;

public class Transmitter implements Callable<String> {
    private String ip;

    public Transmitter(String ip) {
        this.ip = ip;
    }

    @Override
    public String call() throws Exception {
        //TODO заменить sysout на Log4j
        InetAddress ipAddress = InetAddress.getByName(ip); // создаем объект который отображает вышеописанный IP-адрес.
        int serverPort = 6666;
        System.out.println("Try to connect to socket with IP address " + ip + " and port " + serverPort);
        Socket socket = new Socket(ipAddress, serverPort); // создаем сокет используя IP-адрес и порт сервера.
        System.out.println("Connected to " + ip);

        // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиентом.
        InputStream sin = socket.getInputStream();
        OutputStream sout = socket.getOutputStream();

        // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
        DataInputStream in = new DataInputStream(sin);
        DataOutputStream out = new DataOutputStream(sout);

        String line = "All";

        System.out.println("Sending this line to the server...");
        out.writeUTF(line); // отсылаем введенную строку текста серверу.
        out.flush(); // заставляем поток закончить передачу данных.
        line = in.readUTF(); // ждем пока сервер отошлет строку текста.
        System.out.println("The server was very polite. It sent me this : " + line);

        //TODO распарсить и сохранить в бд
        return line;
    }
}
