package by.vsu.bramberry.updateChesker.services;

public interface TransmitterService {
    void transmitAll();

    void transmit(String ipList);
}
