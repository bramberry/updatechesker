package by.vsu.bramberry.updateChesker.services;

import by.vsu.bramberry.updateChesker.model.transfer.Transmitter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class TransmitterServiceImpl implements TransmitterService {

    @Override
    public void transmitAll() {
        int ap = Runtime.getRuntime().availableProcessors();
        ExecutorService es = Executors.newFixedThreadPool(ap);

        ArrayList<String> ipList = new ArrayList<>();//Здесь нужно получить список всех IP
        int STR_COUNT = 100;
        List<Transmitter> threads = new ArrayList<>();

        try {
            for (String ip : ipList) {
                for (int i = 0; i < STR_COUNT; i++) {
                    threads.add(new Transmitter(ip));
                }
            }
            List<Future<String>> list = es.invokeAll(threads);
            es.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace(System.out);
        }
    }

    @Override
    public void transmit(String ip) {
        ExecutorService es = Executors.newSingleThreadExecutor();
        Transmitter transmitter = new Transmitter(ip);
        Future<String> stringFuture = es.submit(transmitter);

    }
}
